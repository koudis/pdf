
# files for clean
M_CLEAN_VAR  =

# where :).
ACTDIR       =



# Functions for init. environment
#
define init_env
$(eval $(call init_env_s))
endef
define init_env_s
ACTDIR := $$(patsubst %/,%,$$(dir $$(lastword $$(MAKEFILE_LIST))))
endef

# include another directory
define include_dir
$(eval $(call include_dir_s,$(1)))
endef
define include_dir_s
$(if $(ACTDIR),include $(ACTDIR)/$(1)/rules.mk,include $(1)/rules.mk)
_tmp:=$$(patsubst %/,%,$$(ACTDIR))
ACTDIR:=$$(dir $$(_tmp))
endef

# install given file
define install_files
$(eval $(call install_s,$(1)))
endef
define install_s
install: $(1)
endef

# install current dir
define install_dir
$(eval $(call install_dir_s))
endef
define install_dir_s
install-dir: $$(ACTDIR)
endef

define add_dep
$(eval $(call add_dep_s,$(1),$(2)))
endef
define add_dep_s
$(ACTDIR)/$(1): $(addprefix $(ACTDIR)/,$(2))
M_CLEAN_VAR += $(addprefix $(ACTDIR)/,$(2))
endef


# Library functions
#
define tex_to_pdf
$(eval $(call tex_to_pdf_s,$(1)))
endef
define tex_to_pdf_s
all: $$(ACTDIR)/$(1).pdf
$$(ACTDIR)/$(1).pdf: $$(ACTDIR)/$(1).tex

M_CLEAN_VAR += $(ACTDIR)/$(1).pdf $(ACTDIR)/$(1).log
$$(call install_files,$(ACTDIR)/$(1).pdf)
$$(call install_dir)
endef

define asy_to_pdf
$(eval $(call asy_to_pdf_s,$(1)))
endef
define asy_to_pdf_s
all: $$(ACTDIR)/$(1).pdf
$$(ACTDIR)/$(1).pdf: $$(ACTDIR)/$(1).asy

M_CLEAN_VAR += $(ACTDIR)/$(1).pdf
$$(call install_files,$(ACTDIR)/$(1).pdf)
$$(call install_dir)
endef

define pdf_to_png
$(eval $(call pdf_to_png_s,$(1),$(2)))
endef
define pdf_to_png_s
ifeq ($(2),)
all: $(ACTDIR)/$(1).png
$(ACTDIR)/$(1).png: $(ACTDIR)/$(1).pdf
M_CLEAN_VAR += $(ACTDIR)/$(1).png
$$(call install_files,$(ACTDIR)/$(1).png)
else
all: $(ACTDIR)/$(2).png
$(ACTDIR)/$(2).png: $(ACTDIR)/$(1).pdf
M_CLEAN_VAR += $(ACTDIR)/$(2).png
$$(call install_files,$(ACTDIR)/$(2).png)
endif

ifeq ($(findstring -crop,$(1)),-crop)
M_CLEAN_VAR += $(ACTDIR)/$(1).pdf
endif

$$(call install_dir)
endef



#
# Robodoupe
#

define rd_init
$(eval $(call rd_include_s,$(1)))
endef
define rd_include_s
$(ACTDIR)/$(1)/%-crop.pdf: $(ACTDIR)/$(1)/%.pdf
	echo Processing $$@
	pdfcrop --margins '1 1 1 1' $$< $$% > /dev/null

$(ACTDIR)/$(1)/%-crop.pdf: $(ACTDIR)/img/%-crop.pdf
	echo Processing $$@
	cp $$< $$@

$(ACTDIR)/$(1)/%.png: $(ACTDIR)/$(1)/%-crop.pdf
	echo Processing $$@
	convert  -density 300 $$< -quality 100 $$@
	convert -resize $$(PNG_RESIZE) -transparent white $$@ $$@_
	mv $$@_ $$@

# resize image to the 12pt line width
$(ACTDIR)/$(1)/%-l.png: $(ACTDIR)/$(1)/%-crop.pdf
	echo Processing $$@
	convert -density 300 $< -quality 100 $$@
	convert -resize $$(PNG_RESIZE_LINE) -transparent white $$@ $$@_
	mv $@_ $@

# resize
$(ACTDIR)/$(1)/%-m.png: $(ACTDIR)/$(1)/%-crop.pdf
	echo Processing $$@
	convert -density 300 $$< -quality 100 $$@
	convert -resize $$(PNG_RESIZE_MEDIUM) -transparent white $$@ $$@_
	mv $$@_ $$@

$(ACTDIR)/$(1)/%.png: $(ACTDIR)/$(1)/%-crop.pdf
	echo Processing $$@
	convert -density 200 $$< -quality 100 $$@

$$(call include_dir,$(1))

endef



# rd_add_image($1)
# $1 - image name (for lineq.png --> lineq)
# $2 - image format
#         l - line
#         m - medium
# $2 is empty, then size modifications will not be applied
#
define rd_add_image
$(ACTDIR)/$(1).pdf: $(ACTDIR)/$(1).tex
_tmp=$(if $(2),-$(2),)
$(ACTDIR)/robodoupe/$(1)$$(_tmp).png: $(ACTDIR)/$(1)-crop.pdf
robodoupe: $(ACTDIR)/$(1)$$(_tmp).png
endef
define rd_add_image
$(eval $(call _rd_add_image,$(1),$(2)))
endef
