
import fontsize;
size(200mm, 0);



struct s_vector {
	path  v;
	Label label;
}

real default_vector_length = 400;

s_vector create_vector(transform s = shift((0,0)),
  pair endpoint = (1,0), Label label = "$v$", real length = default_vector_length) {
	s_vector _v;
	_v.v     = s*((0, 0)--(length*endpoint));
	_v.label = Label(label);
	return _v;
}

void draw_vector(s_vector v, pen p) {
	p = p + 6*linewidth(p) + fontsize(36pt);
	draw(v.v, p = p + 0.5*linewidth(p),
		arrow=Arrow(SimpleHead),
		L = Label(v.label)
	);
}

void draw_vector(s_vector v[], pen p) {
	int length = v.length;
	for(int i; i < length; ++i) {
		draw_vector(v[i], p);
	}
}



void draw_right_angle(pen p, transform s = shift(0,0), real length = 15) {
	path _a    = (0,0)--(0,length);
	path _b    = (0,0)--(length,0);
	pair end = relpoint(_a, 1);
	pair start   = relpoint(_b, 1);

	path arc  = s*arc((0,0), start, end);
	path circ = s*circle((0.4*length,0.4*length), linewidth(p));

	draw(arc, p = p);
	fill(circ, p);
	draw(circ, p = p + 3*linewidth(p));
}



void draw_0xy(pen p, transform s) {
	p = p + 10*linewidth(p);
	Label Lx = Label("$x$", s*Relative(1));
	Label Ly = Label("$y$", s*Relative(1));
	Label Ls = Label("$0$", s*Relative(0));
	path y = (0,0)--(0,1000);
	path x = (0,0)--(1000,0);

	label(Lx, x,
		align = LeftSide,
		p     = p + fontsize(36pt)
	);
	label(Ly, y,
		align = RightSide,
		p     = p + fontsize(36pt)
	);
	label(Ls, y,
		align = (0,-1.5),
		p     = p + fontsize(36pt) 
	);

	real arrow_length = 10 * linewidth(p);
	arrow(b = s*(1000 + arrow_length,0), dir = (-1,0),
		length = arrow_length,
		p      = p,
		arrow  = Arrow(SimpleHead)
	);
	arrow(b = s*(0, 1000 + arrow_length), dir = (0,-1),
		length = arrow_length,
		p      = p,
		arrow  = Arrow(SimpleHead)
	);

	dot((0,0), p = p + 2.5*linewidth(p));
	draw(s*x, p = p);
	draw(s*y, p = p);
}
