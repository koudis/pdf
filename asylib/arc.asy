
path on_intersec(path a, path b, real r) {
	real[] isec = intersect(a, b);
	pair center = (isec[0], isec[1]);
	pair start  = relpoint(a, r);
	pair end    = relpoint(b, r);
	return arc(center, start, end);
}
