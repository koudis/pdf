include main.mk

.SILENT:
$(call init_env)
$(call include_dir,podvozek)

.DEFAULT_GOAL: all



PDFCSPLAIN = pdfcsplain
OUTPUT_DIR = web

TEXINPUTS:=$(TEXINPUTS):./
export TEXINPUTS



PNG_RESIZE = 400x400

%-crop.pdf: %.pdf
	echo Processing $@
	pdfcrop --margins '1 1 1 1' $< $% > /dev/null

%.png: %-crop.pdf
	echo Processing $@
	convert -density 300 $< -quality 100 $@

%.pdf: %.asy
	echo Processing $@
	asy -f pdf $<
	mv $(notdir $@) $(dir $@)

%.pdf: %.tex
	echo Processing $@
	cd $(dir $@) && \
	$(PDFCSPLAIN) $(notdir $<)

install-dir:
	mkdir -p $(addprefix $(OUTPUT_DIR)/,$^)

install:
	mkdir -p $(OUTPUT_DIR)
	cp -r --parents $^ $(OUTPUT_DIR)

clean:
	rm -rf $(M_CLEAN_VAR)
	rm -rf web

