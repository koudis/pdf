#!/usr/bin/env python
from __future__ import division

import numpy as np
import math
import sys


V     = 200;
alpha = float(sys.argv[1]);
P     = float(sys.argv[2]);
k_s   = float(sys.argv[3]);
S     = float(sys.argv[4]);

if((1-k_s)*V < math.fabs(S)):
    print "'S' out of range"
    sys.exit(1);



A = np.matrix([
    [2,  0,             1],
    [-1, math.sqrt(3),  1],
    [-1, -math.sqrt(3), 1]]);

z = np.matrix([
    [1],
    [1],
    [1]]);

s = np.matrix([
    [math.cos(alpha)],
    [math.sin(alpha)],
    [0]]);

Ain = A**-1;



v = (A*s);
v_length = np.linalg.norm(v);

u_v  = (1/v_length) * v;
v_r  = P*V*k_s*u_v + S*z;
s_vr = Ain*v_r;

print "s:\n", s;
print "\nv_r:\n", v_r;
print "\ns from v_r:\n", s_vr;
print "\ns_vr[0]/s_vr[1], s[0]/s[1]", s_vr[0] / s_vr[1], s[0] / s[1];

check = round((s[0]/s[1]) - (s_vr[0]/s_vr[1]), 6);
if(not check):
    print "\nOK\n";
else:
    print "Not works!\n";
