#!/usr/bin/env python
from __future__ import division

import numpy as np
import math
import sys



S     = 50
alpha = float(sys.argv[1])

A_ = np.matrix([
    [2,  0,             1],
    [-1, math.sqrt(3),  1],
    [-1, -math.sqrt(3), 1]]);

z = np.matrix([
    [1],
    [1],
    [1]]);

s = np.matrix([
    [math.cos(alpha)],
    [math.sin(alpha)],
    [0]]);

Ain   = A_**-1;



print np.linalg.norm(A_*s) / math.sqrt(6);

s_tmp = Ain*(A_*s + S*z);
print s_tmp[0], s_tmp[1], s_tmp[2];
