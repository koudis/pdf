
access "./asylib/vector.asy" as vector;
access "./asylib/arc.asy"    as arc;

from "./asylib/vector.asy" access s_vector;



struct s_wheel {
	path  axis;
	path  wheel;
	Label label;
}

s_wheel wheel(real angle = 0, real wheel_size = 100, Label label = "") {
	s_wheel w;
	w.label = label;
	w.axis  = (0,0)--(0,1000);
	w.axis= rotate(angle)*(w.axis);
	w.wheel = (-wheel_size,1000)--(wheel_size,1000);
	w.wheel = rotate(angle)*(w.wheel);
	return w;
}

void draw_wheel(s_wheel w, pen[] p) {
	draw(w.axis, p[0] + 6*linewidth(p[0]) + linecap(0) + fontsize(36pt), L = w.label);
	draw(w.wheel, p[1] + 10*linecap(p[1]) + linecap(0));
}

void draw_wheel(s_wheel w[], pen[] p) {
	real length = w.length;
	for(int i = 0; i < length; ++i)	{
		draw_wheel(w[i], p);
	}
}




//
// Draw chassis
//
//
picture chassis;
currentpicture = chassis;
size(300mm);

pen default = currentpen;
pen wheel = default;
pen axis  = default;
pen xy    = default + rgb(0, 0, 255);
pen dim   = default + gray(0.75) + 6*linewidth(default) + fontsize(36pt);

transform s = scale(0.5);

// define wheels
s_wheel[] wheels = {
	wheel(240, label = Label("$3$", Relative(1))),
	wheel(120, label = Label("$2$", Relative(1))),
	wheel(0, label = Label("$1$", Relative(1)))
};

// Draw arc
path arc = arc.on_intersec(wheels[2].axis, wheels[1].axis, 0.6);
draw(arc, p=dim, arrow=ArcArrows(SimpleHead), L = Label("$120^o$", Relative(0.5), align=LeftSide));

// draw wheels
pen _p[] = {axis, wheel};
draw_wheel(wheels, _p);

// draw coordinate system
vector.draw_0xy(xy, scale(0.5));

// Draw vectors
transform v_shift = shift((0,1000));
s_vector[] vectors = {
	vector.create_vector(s = shift((0,1000)),label = Label("$v_1$", Relative(1), align = RightSide)),
	vector.create_vector(s = rotate(120)*v_shift, label = Label("$v_2$", Relative(1), align = RightSide)),
	vector.create_vector(s = rotate(240)*v_shift, label = Label("$v_3$", Relative(1), align = RightSide))
};
vector.draw_vector(vectors, p = default + gray(0.75));

// Draw vector S
s_vector s = vector.create_vector(s = rotate(37),
	label = Label("$s$", Relative(1), align = LeftSide),
	length = 1000
);
vector.draw_vector(s, p = default + gray(0.75));

frame chassis_frame = chassis.fit();

shipout(chassis_frame);
