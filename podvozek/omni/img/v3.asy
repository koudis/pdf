
access "./asylib/vector.asy" as vector;
access "./asylib/arc.asy"    as arc;

from "./asylib/vector.asy" access s_vector;


//
// Draw vector_1
//
//
picture v3;
currentpicture = v3;
size(200mm, 0);
pen default = currentpen;

vector.default_vector_length = 200;

dot((0,0), p = default + 3*linewidth(default));

s_vector a = vector.create_vector(endpoint = (0, -1),
	label    = "$\vec{v}_3$",
	s        = rotate(-30)
);
s_vector b = vector.create_vector(endpoint = (0, -1),
	label    = "$\vec{v}_{3y}$",
	length   = vector.default_vector_length*cos(pi/6)
);
s_vector c = vector.create_vector(endpoint = (-1, 0),
	label    = Label("$\vec{v}_{3x}$"),
	length   = vector.default_vector_length*sin(pi/6),
	s        = shift((0, -vector.default_vector_length*cos(pi/6)))
);

path arc = arc.on_intersec(a.v, b.v, 0.3);
draw(arc, L = Label("$\alpha$", align = LeftSide), p = default + fontsize(36pt));

vector.draw_vector(a, p = default + 2*linewidth(default));
vector.draw_vector(b, p = default);
vector.draw_vector(c, p = default);

//dot((0,0), default + 7*linewidth(default));

vector.draw_right_angle(p = default,
	s = shift(0, -vector.default_vector_length*cos(pi/6))*rotate(90)
);

label(
	L = minipage(
		"\begin{tabular}{ r c l }
  			$\vec{v}_{3x}$ & $=$ & $(-|v_3| sin(\alpha),0)$ \\
  			$\vec{v}_{3y}$ & $=$ & $(0,-|v_3| cos(\alpha))$ \\
		\end{tabular}"
	),
	p = default + fontsize(36pt),
	position = (-180, -40)
);

frame v3_frame = v3.fit();
shipout(v3_frame);
