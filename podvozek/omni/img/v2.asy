
import "./asylib/vector.asy" as vector;
access "./asylib/arc.asy"    as arc;

//
// Draw vector_2
//
//
picture v2;
currentpicture = v2;
size(200mm, 0);
pen default = currentpen;

default_vector_length = 200;

dot((0,0), p = default + 3*linewidth(default));

s_vector a = create_vector(endpoint = (0, 1),
	label    = Label("$\vec{v}_2$", align = LeftSide),
	s        = rotate(30)
);
s_vector b = create_vector(endpoint = (0, 1),
	label    = "$\vec{v}_{2y}$",
	length   = default_vector_length*cos(pi/6)
);
s_vector c = create_vector(endpoint = (-1, 0),
	label    = Label("$\vec{v}_{2x}$"),
	length   = default_vector_length*sin(pi/6),
	s        = shift((0, default_vector_length*cos(pi/6)))
);

path arc = arc.on_intersec(b.v, a.v, 0.3);
draw(arc, L = Label("$\alpha$", align = LeftSide), p = default + fontsize(36pt));

draw_vector(a, p = default + 2*linewidth(default));
draw_vector(b, p = default);
draw_vector(c, p = default);

//dot((0,0), default + 7*linewidth(default));

draw_right_angle(p = default,
	s = shift(0, default_vector_length*cos(pi/6))*rotate(180)
);

label(
	L = minipage(
		"\begin{tabular}{ r c l }
  			$\vec{v}_{2x}$ & $=$ & $(-|v_2| sin(\alpha),0)$ \\
  			$\vec{v}_{2y}$ & $=$ & $(0,|v_2| cos(\alpha))$ \\
		\end{tabular}"
	),
	p = default + fontsize(36pt),
	position = (-180, 40)
);

frame v2_frame = v2.fit();
