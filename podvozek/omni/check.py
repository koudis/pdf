#!/usr/bin/env python
from __future__ import division

import numpy as np
import math
import sys



A_ = np.matrix([
    [2,  0,             1],
    [-1, math.sqrt(3),  1],
    [-1, -math.sqrt(3), 1]]);

v_ = np.matrix([
    [1, -0.5,           -0.5],
    [0, math.sqrt(3)/2, -math.sqrt(3)/2]
]);

s = np.matrix([
    [float(sys.argv[1])],
    [float(sys.argv[2])],
    [float(sys.argv[3])]
]);

k = A_*s;
v = v_*k;


print "s[0:3]:\t\t",   np.squeeze(np.asarray(s));
print "v[0:2]:\t\t",   np.squeeze(np.asarray(k));
print "s[0]/s[1]:\t", s[0]/s[1];
print "v[0]/v[1]:\t", v[0]/v[1];

check = round((s[0]/s[1]) - (v[0]/v[1]), 6);
if(not check):
    print "OK";
else:
    print "NOT WORKS!";
