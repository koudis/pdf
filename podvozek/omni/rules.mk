$(call init_env)

$(call tex_to_pdf,omniwheel1)

$(call asy_to_pdf,img/chassis)
$(call asy_to_pdf,img/v2)
$(call asy_to_pdf,img/v3)

$(call add_dep,omniwheel1.pdf, img/chassis.pdf img/v2.pdf img/v3.pdf)



#
# Robodoupe
#

$(call pdf_to_png,img/chassis-crop,robodoupe/chassis)
$(call rd_init,robodoupe)
