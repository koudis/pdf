#!/usr/bin/env python
from __future__ import division

import numpy as np
import math
import sys


V     = 255;
k_s   = 1.0;
M_max = 255
alpha = float(sys.argv[1]);
M     = float(sys.argv[2]);
X     = float(sys.argv[3]);
S     = float(sys.argv[4]);

if((1-k_s)*V < math.fabs(S)):
    print "'S' out of range"
    sys.exit(1);

def Vfunc(m):
    if(m <= X):
        return 0
    if(m >= M_max):
        return V
    return (m/M_max)*(k_s*V);



A = np.matrix([
    [2,  0,             1],
    [-1, math.sqrt(3),  1],
    [-1, -math.sqrt(3), 1]]);

z = np.matrix([
    [1],
    [1],
    [1]]);

s = np.matrix([
    [math.cos(math.pi*(alpha/180))],
    [math.sin(math.pi*(alpha/180))],
    [0]]);

Ain = A**-1;



v = (A*s);
v_length = np.linalg.norm(v);

u_v  = (1/v_length) * v;
v_r  = Vfunc(M)*u_v + S*z;

print "s:\n", s;
print "\nv_r:\n", round(v_r[0], 6), round(v_r[1], 6), round(v_r[2], 6);
